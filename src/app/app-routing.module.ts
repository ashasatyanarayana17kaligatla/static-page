import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PerformanceComponent } from './components/performance/performance.component';
import { ContactComponent } from './components/contact/contact.component';
import { BlankComponent } from './components/blank/blank.component';
import { HeaderComponent } from './header/header.component';
import { InputPageComponent } from './components/input-page/input-page.component';

const routes: Routes = [
   { path: '', component: HeaderComponent },
  //  { path: '',redirectTo: '/home', pathMatch: 'full'  },
  {
    path: 'home',
    component: HomeComponent
    // children: [
    //   { path: 'input-page', component: InputPageComponent }
    // ]
  },
  { path: 'input-page', component: InputPageComponent },
  { path: 'performance', component: PerformanceComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'blank', component: BlankComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HeaderComponent,
  HomeComponent,
  InputPageComponent,
  PerformanceComponent,
  ContactComponent,
  BlankComponent
]