import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { PerformanceComponent } from './components/performance/performance.component';
import { ContactComponent } from './components/contact/contact.component';
import { BlankComponent } from './components/blank/blank.component';
import { Screen1Component } from './screens/screen1/screen1.component';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { HeaderComponent } from './header/header.component';
import { Screen2Component } from './screens/spiliter1/screen2.component';
import {SplitterModule} from 'primeng/splitter';
import { InputPageComponent } from './components/input-page/input-page.component';
import { NavButtonComponent } from './components/nav-button/nav-button.component';
import { FormsModule } from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PerformanceComponent,
    ContactComponent,
    BlankComponent,
    Screen1Component,
    HeaderComponent,
    Screen2Component,
    InputPageComponent,
    NavButtonComponent,
  
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ToolbarModule,
    ButtonModule,
    SplitterModule,
    FormsModule,
    InputTextModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
