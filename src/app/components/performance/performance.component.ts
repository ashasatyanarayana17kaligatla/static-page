import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.css']
})
export class PerformanceComponent implements OnInit {
  name1:any;
  name2:any;
    constructor() { }
  
    ngOnInit(): void {
      this.name1="cat";
      this.name2="dog";
    }
  
}
