import { Component, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-screen2',
  templateUrl: './screen2.component.html',
  styleUrls: ['./screen2.component.css'],
  // inputs:[`parentData`]
  })
export class Screen2Component implements OnInit {
// @Input() public parentData:any;
enabletext: boolean = false;
@Input() name1: any;
@Input() name2: any;
value1:any;
  constructor() { }

  ngOnInit(): void {
    if(this.name2 === "phone"){
      this.enabletext = true
    }
  }
}
